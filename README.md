singleton-module-spike
======================

The Node.js application creates a cookie in the first user's browser, unless it
already exists.  It then passes the cookie value to the "session manager" on
the server side.

When the second user accesses the application, the above scenario gets
repeated. But because the session manager follows the singleton design pattern,
its state gets overridden by the cookie from the second user.

That means different users keep mutating each other's sessions because they were
intended to be specific to themselves, rather than a server instance (which is
in fact the case).
