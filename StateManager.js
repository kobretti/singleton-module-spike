let sessions
let currentSessionId

class StateManager {
  constructor() {
    sessions = {}

    this.setSessionId('default-value')
  }

  getCurrentSessionId() {
    return currentSessionId
  }

  setSessionId(sessionId) {
    currentSessionId = sessionId

    if (sessions[currentSessionId]) {
      return
    }
    sessions[currentSessionId] = {}
  }
}

module.exports = new StateManager()
