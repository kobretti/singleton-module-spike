const Hapi = require('hapi')
const goodPlugin = require('good')
const stateManager = require('./StateManager')

const server = new Hapi.Server()
server.connection({
  //host: 'localhost',
  port: 3200
})

const options = {
  reporters: {
    console: [
      {
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{
          log: ['error', 'warn'],
          response: '*'
        }]
      }, {
        module: 'good-console',
        args: [{ format: 'YYYYMMDD/HHmmss.SSS' }]
      },
      'stdout'
    ]
  }
}

server.register({
  register: goodPlugin,
  options
}, loggingError => {

  server.route({
    method: 'GET',
    path: '/{name}',
    handler: function (request, reply) {
      console.log('user cookie: ', request.state.userSession)
      console.log('server session: ', stateManager.getCurrentSessionId())
      if (request.state.userSession) {
        reply(`<p>Server session: ${stateManager.getCurrentSessionId()}<br/>
            User cookie: ${request.state.userSession}</p>`)
          .type('text/html')
        return
      }
      const currentTime = +new Date()
      reply.state('userSession', `${currentTime}`, { isSecure: false })
      // This sets SERVER, not USER session!
      stateManager.setSessionId(currentTime)

      reply(`<p>Server session created: ${stateManager.getCurrentSessionId()}
          <br/>
          User cookie: ${request.state.userSession}</p>`)
        .type('text/html')
    }
  })

  // Ignore favicon requests for clarity
  server.route({
    method: 'GET',
    path: '/favicon.ico',
    handler: function (request, reply) {
      reply.close()
    }
  })

  // Start the server
  server.start(error => {
    if (error) {
      throw error
    }
    console.log('Server running at:', server.info.uri)
  })
})
